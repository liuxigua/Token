address 0x2245249C6f16343CDcf780ec4Fec36a8 {
module MyToken {
    use 0x1::Token;
    use 0x1::Account;

    struct MyToken has copy, drop, store { }

    public(script) fun init_token(account: &signer) {
        Token::register_token<MyToken>(account, 3);
        Account::do_accept_token<MyToken>(account);
    }

    public(script) fun mint_token(account: &signer, amount: u128) {
       let token = Token::mint<MyToken>(account, amount);
       Account::deposit_to_self<MyToken>(account, token)
    }
}
}
